#!/bin/bash

for version in */ ; do
     tag=${version::-1}
     docker build -t registry.gitlab.com/docker-images-build/php-ms-sql:$tag $version
     docker push registry.gitlab.com/docker-images-build/php-ms-sql:$tag
done