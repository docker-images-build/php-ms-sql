FROM php:7.3-cli-stretch

ENV ACCEPT_EULA=Y
ENV EXT_AMQP_VERSION=master
ENV COMPOSER_HOME=/composer

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       locales \
       apt-transport-https \
       gnupg \
    && curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl https://packages.microsoft.com/config/debian/9/prod.list \
        > /etc/apt/sources.list.d/mssql-release.list \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
        unixodbc-dev \
        msodbcsql17 \
        libcurl4-gnutls-dev \
        libpng-dev \
        zlib1g-dev \
        libicu-dev \
        g++ \
        libxml2-dev \
        libzip-dev \
        git \
        librabbitmq-dev \
        openssh-client \
        rsync

# Install amqp extension
RUN docker-php-source extract && \
   mkdir -p /usr/src/php/ext/amqp && \
   curl -L https://github.com/php-amqp/php-amqp/archive/master.tar.gz | tar -xzC /usr/src/php/ext/amqp --strip-components=1 && \
   docker-php-ext-install amqp && \
   docker-php-ext-enable amqp && \
   docker-php-source delete

# Install PHP extensions
RUN docker-php-ext-install mbstring pdo pdo_mysql bcmath curl gd intl json mbstring soap xml zip gd\
    && pecl install sqlsrv pdo_sqlsrv xdebug \
    && docker-php-ext-enable sqlsrv pdo_sqlsrv xdebug bcmath curl gd intl json soap xml zip gd

# Install composer
RUN curl https://getcomposer.org/installer > composer-setup.php \
	&& php composer-setup.php \
	&& mv composer.phar /usr/local/bin/composer \
	&& rm composer-setup.php

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*